package main

import (
	"context"
	"flag"
	"fmt"
	"log"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

var (
	kubeconfig = flag.String("kubeconfig", "/home/ashwani/work/netbook/azcluster", "absolute path to the kubeconfig file")
)

func main() {

	flag.Parse()
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		log.Fatal(err.Error())
	}
	clientset, _ := kubernetes.NewForConfig(config)
	watch, err := clientset.CoreV1().Events("netbook").Watch(context.TODO(), metav1.ListOptions{TypeMeta: metav1.TypeMeta{Kind: "Workspace"}})
	if err != nil {
		log.Fatal(err)
	}

	for item := range watch.ResultChan() {
		fmt.Println(item.Object)

		fmt.Println()
	}

}
